//
//  School.swift
//  NYCSchools
//
//  Created by kalyan  on 7/25/22.
//

import Foundation

struct School: Decodable {
    let dbn: String
    let school_name: String
    let boro: String
    let overview_paragraph: String
}
