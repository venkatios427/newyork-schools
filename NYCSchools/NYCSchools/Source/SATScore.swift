//
//  SATScore.swift
//  NYCSchools
//
//  Created by kalyan  on 7/25/22.
//

import Foundation

struct SATScore: Decodable {
    let dbn: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
}
