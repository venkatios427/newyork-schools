//
//  SchoolViewModel.swift
//  NYCSchools
//
//  Created by kalyan  on 7/25/22.
//

import Foundation
import UIKit

struct SchoolViewModel {
    
    let id: String
    let name: String
    let detailTextString: String
    let accessoryType: UITableViewCell.AccessoryType
    
    
    init(school: School) {
        
        self.id = school.dbn
        self.name = school.school_name
        self.detailTextString = school.overview_paragraph
        self.accessoryType = .detailDisclosureButton
    }
}
