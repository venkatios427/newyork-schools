//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by kalyan  on 7/25/22.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    
    var school: SchoolViewModel?
    var scores: SATScore?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = self.school?.name {
            self.schoolLabel?.text = name
        }
        
        if let scores = self.scores {
            if Int(scores.sat_math_avg_score)! > 0 {
                self.mathLabel?.text = "MATH: \(scores.sat_math_avg_score)"
            } else {
                self.mathLabel?.text = "N/A"
            }
            
            if Int(scores.sat_critical_reading_avg_score)! > 0 {
                self.readingLabel?.text = "READ: \(scores.sat_critical_reading_avg_score)"
            } else {
                self.readingLabel?.text = "N/A"
            }
            
            if Int(scores.sat_math_avg_score)! > 0 {
                self.writingLabel?.text = "WRITE: \(scores.sat_writing_avg_score)"
            } else {
                self.writingLabel?.text = "N/A"
            }
        }
        
    }
}
